import json
import pandas as pd 
import numpy as np 
import argparse as ap 

def pandas_table(test):
	with open(f'./energy_exp_{test}.json', 'r') as f:
		runs = json.load(f) 
	

	pd.set_option('display.max_rows', None)
	pd.set_option('display.max_columns', None)
	pd.set_option('display.width', 2000)
	#pd.set_option('display.float_format', '{:.2f}'.format)
	pd.set_option('display.max_colwidth', None)
	df = pd.DataFrame()
	for i, run in enumerate(runs):
		#df[f"{run['full']},{run['redu']}"] = run['ener']
		df[f"{run['full']},{run['redu']}"] = list(zip(np.log10(run['errs']).astype(int), run['ts']))

	print()
	print(test)
	print
	print(df) 
	"""
	dataframe layout:
	# precision1,             precision2,           ..., precisionn
	n (err_order, run_time), (err_order, run_time), ...,
	"""

def main():
	parser = ap.ArgumentParser(description='Plot the results of the tests!')
	parser.add_argument('test', metavar='T', type=str, help='Name of the test to show (dir name)')
	args = parser.parse_args()
	pandas_table(args.test)

if __name__ == '__main__':
	main()