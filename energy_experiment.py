import subprocess as sp 
import os
import numpy as np 
#import matplotlib.pyplot as plt 
import json
import itertools
#import multiprocessing as mp 
import energyusage as eu 
import time 

"""
Compile using included makefile
d_redu - The reduced precision data type {real32, real64, real128}
d_full - The full precision data type {same ^}

no return

Note: I did try using subprocess but make wouldn't use the variables.
so I just used os.system
"""
def compile(test_name, d_full, d_redu):
	cmd = f"cd {test_name} && make fp='{d_full}' rp='{d_redu}' && cd .."
	print(cmd)
	#print(cmd.split())
	#sp.run(['make', f"full_prec='{d_full}'", f"redu_prec='{d_redu}'"], shell=True, check=True)
	#sp.run(cmd.split(), shell=True, check=True)
	os.system(cmd)

"""
Run the program
dt - Program argument for dt

program outputs the time to run the method, so capture it and return
"""
def run_prog(dt, test_name, n_runs=5):
	t = 0
	err = 0
	en = 0
	power = 0
	for i in range(n_runs):
		out = sp.run([f'./run_powercap.x', f'{test_name}', f'{dt}'], capture_output=True)
		#print(out.stdout.split())
		lines = out.stdout.split()
		t = t + float(lines[2])
		err = err + float(lines[5])
		if ("1p" in test_name) or ("2p" in test_name) or ("3p" in test_name):
			#print(lines[7])
			en = en + float(lines[11]) #joules
			#print(lines[11][:-2])
			power = power + float(lines[15][:-2]) #watts
		else:
			#print(lines[7])
			en = en + float(lines[7]) #joules
			#print(lines[11][:-2])
			power = power + float(lines[11][:-2]) #watts
		time.sleep(10)
	return t/n_runs, err/n_runs, en/n_runs, power/n_runs

"""
Run the full experiment for one of the tests
test_name - the name of the test to run. Name must match the directory 
and teh name of the test.

Writes the results of the experiment inside the test directory to a 
json file
"""
def experiment(test_name):

	dt_exponent_min = 1
	dt_exponent_max = 7

	d_types = ['real128', 'real64', 'real32']
	#d_types = ['real64']
	combos = itertools.combinations_with_replacement(d_types, 2)
	
	runs = []
	for d_full, d_redu in combos:
		compile(test_name, d_full, d_redu)
		time.sleep(10)
		r = {'redu': d_redu, 'full': d_full, 'errs': [], 'ts': [], 'ener': [], 'pow': []}
		for exponent in range(dt_exponent_min, dt_exponent_max+1):
			dt = 10**(-exponent)
			print(f"Running {test_name} for dt={dt}")
			t, e, en, power = run_prog(dt, test_name)
			r['ener'].append(en)
			r['errs'].append(e)
			r['ts'].append(t)
			r['pow'].append(power)
		runs.append(r)

	with open(f'energy_exp_{test_name}.json', 'w') as f:
		json.dump(runs, f)

def main():
	#tests = ['impmid', 'eff-impmid', 'sdirk', 'eff-sdirk']
	#tests = ['impmid', 'eff-impmid', 'sdirk', 'eff-sdirk', 'novel-4s3pA', 'novel-4s3pB', 'novel-4s3pC']
	#tests = ['impmid-1p', 'impmid-2p', 'sdirk-1p', 'sdirk-2p', 'sdirk-3p']
	tests = ['impmid-1p', 'impmid-2p']
	print("running tests:")
	print(tests)
	time.sleep(1)
	for test in tests:
		experiment(test)

if __name__ == '__main__':
	main()