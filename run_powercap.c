/**
 * @author PAPI team UTK/ICL
 * Test case for powercap component
 * @brief
 *   Tests basic functionality of powercap component
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "papi.h"

#define MAX_powercap_EVENTS 64
#define TESTS_QUIET 0

#define MATRIX_SIZE 1024
static double a[MATRIX_SIZE][MATRIX_SIZE];
static double b[MATRIX_SIZE][MATRIX_SIZE];
static double c[MATRIX_SIZE][MATRIX_SIZE];

/* Naive matrix multiply */
void run_test( int quiet )
{
    double s;
    int i,j,k;

    if ( !quiet ) printf( "Doing a naive %dx%d MMM...\n",MATRIX_SIZE,MATRIX_SIZE );

    for( i=0; i<MATRIX_SIZE; i++ ) {
        for( j=0; j<MATRIX_SIZE; j++ ) {
            a[i][j]=( double )i*( double )j;
            b[i][j]=( double )i/( double )( j+5 );
        }
    }

    for( j=0; j<MATRIX_SIZE; j++ ) {
        for( i=0; i<MATRIX_SIZE; i++ ) {
            s=0;
            for( k=0; k<MATRIX_SIZE; k++ ) {
                s+=a[i][k]*b[k][j];
            }
            c[i][j] = s;
        }
    }

    s=0.0;
    for( i=0; i<MATRIX_SIZE; i++ ) {
        for( j=0; j<MATRIX_SIZE; j++ ) {
            s+=c[i][j];
        }
    }

    if ( !quiet ) printf( "Matrix multiply sum: s=%lf\n",s );
}


int main ( int argc, char **argv )
{
    int retval,cid,powercap_cid=-1,numcmp;
    int EventSet = PAPI_NULL;
    long long *values;
    int num_events=0;
    int code;
    char event_names[MAX_powercap_EVENTS][PAPI_MAX_STR_LEN];
    char event_descrs[MAX_powercap_EVENTS][PAPI_MAX_STR_LEN];
    char units[MAX_powercap_EVENTS][PAPI_MIN_STR_LEN];
    int data_type[MAX_powercap_EVENTS];
    int r,i;
    char cmdstr[128];

    if (argc != 3)
    {
        printf("usage: ./run_powercap.x {test_name} {dt}\n");
        printf("test_name: name of directory/executable of test. Use outside test directory\n");
        printf("dt: size of time step for test");
        exit(0);
    }
    cmdstr[0] = '\0';
    strcat(cmdstr, "cd ");
    strcat(cmdstr, argv[1]);
    strcat(cmdstr, " && ./");
    strcat(cmdstr, argv[1]);
    strcat(cmdstr, ".x");
    strcat(cmdstr, " ");
    strcat(cmdstr, argv[2]);
    //printf("%s\n", cmdstr);

    const PAPI_component_info_t *cmpinfo = NULL;
    PAPI_event_info_t evinfo;
    long long before_time,after_time;
    double elapsed_time;

    /* PAPI Initialization */
    retval = PAPI_library_init( PAPI_VER_CURRENT );
    if ( retval != PAPI_VER_CURRENT )
        printf("PAPI_library_init failed: %d\n",retval );

    //printf( "Trying all powercap events\n" );

    numcmp = PAPI_num_components();

    for( cid=0; cid<numcmp; cid++ ) {

        if ( ( cmpinfo = PAPI_get_component_info( cid ) ) == NULL )
            printf("PAPI_get_component_info failed\n");

        if ( strstr( cmpinfo->name,"powercap" ) ) {
            powercap_cid=cid;
            //printf( "Found powercap component at cid %d\n",powercap_cid );
            if ( cmpinfo->disabled ) {
                if ( !TESTS_QUIET ) {
                    printf( "powercap component disabled: %s\n", cmpinfo->disabled_reason );
                }
                printf("powercap component disabled\n");
            }
            break;
        }
    }

    /* Component not found */
    if ( cid==numcmp )
        printf("No powercap component found\n");

    /* Skip if component has no counters */
    if ( cmpinfo->num_cntrs==0 )
        printf("No counters in the powercap component\n");

    /* Create EventSet */
    retval = PAPI_create_eventset( &EventSet );
    if ( retval != PAPI_OK )
        printf( "PAPI_create_eventset(): %d\n",retval );

    /* Add all events */
    code = PAPI_NATIVE_MASK;
    r = PAPI_enum_cmp_event( &code, PAPI_ENUM_FIRST, powercap_cid );
    while ( r == PAPI_OK ) {
        retval = PAPI_event_code_to_name( code, event_names[num_events] );
        if ( retval != PAPI_OK )
            printf("Error from PAPI_event_code_to_name: %d\n", retval );

        retval = PAPI_get_event_info( code,&evinfo );
        if ( retval != PAPI_OK )
            printf( "Error getting event info: %d\n",retval );

        strncpy( event_descrs[num_events],evinfo.long_descr,sizeof( event_descrs[0] )-1 );
        strncpy( units[num_events],evinfo.units,sizeof( units[0] )-1 );
        // buffer must be null terminated to safely use strstr operation on it below
        units[num_events][sizeof( units[0] )-1] = '\0';
        data_type[num_events] = evinfo.data_type;
        retval = PAPI_add_event( EventSet, code );

        if ( retval != PAPI_OK )
            break; /* We've hit an event limit */
        num_events++;

        r = PAPI_enum_cmp_event( &code, PAPI_ENUM_EVENTS, powercap_cid );
    }

    values=calloc( num_events,sizeof( long long ) );
    if ( values==NULL )
        printf("No memory: %d\n",retval );
    
    //printf( "\nStarting measurements...\n\n" );

    /* Start Counting */
    before_time=PAPI_get_real_nsec();
    retval = PAPI_start( EventSet );
    if ( retval != PAPI_OK )
        printf( "PAPI_start(): %d\n",retval );

    /* Run test */
    //run_test( TESTS_QUIET );
    int test_status = 0;
    test_status = system(cmdstr);
    if (test_status != 0)
        printf("System call returned %d\n", test_status);

    /* Stop Counting */
    after_time=PAPI_get_real_nsec();
    retval = PAPI_stop( EventSet, values );
    if ( retval != PAPI_OK )
        printf( "PAPI_stop(): %d\n",retval );

    elapsed_time=( ( double )( after_time-before_time ) )/1.0e9;

    //printf( "\nStopping measurements, took %.3fs, gathering results...\n\n", elapsed_time );

    //printf( "\n" );
    //printf( "scaled energy measurements:\n" );
    for( i=0; i<num_events; i++ ) {
        if ( strstr( event_names[i],"ENERGY_UJ" ) ) {
            if ( data_type[i] == PAPI_DATATYPE_UINT64 ) {
                printf( "%-45s%-20s%4.6f J (Average Power %.1fW)\n",
                        event_names[i], event_descrs[i],
                        ( double )values[i]/1.0e6,
                        ( ( double )values[i]/1.0e6 )/elapsed_time );
            }
        }
    }


    /* Done, clean up */
    retval = PAPI_cleanup_eventset( EventSet );
    if ( retval != PAPI_OK )
        printf("PAPI_cleanup_eventset(): %d\n",retval );

    retval = PAPI_destroy_eventset( &EventSet );
    if ( retval != PAPI_OK )
        printf( "PAPI_destroy_eventset(): %d\n",retval );

    return 0;
}