!gfortran novel-4s3pB.f08 -o novel-4s3pB -cpp -DFULL_TYPE=real128 -DREDU_TYPE=real32
! Full and reduced precision type defaults
#ifndef FULL_TYPE
#define FULL_TYPE real64
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real32
#endif

program main
	!Declare the full and reduced precision type names.
	!remember for literal constants to put 1.0_fp to cast correctly
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE, qp=>real128
	implicit none
	character(len=32) :: arg
	!dt, initial time, final time, a
	real(fp) :: dt, ti=0.0, tf=1.0, a=1.0
	real :: start, finish !start/finish times for method
	real(fp) :: y0(2) !initial condition
	!ys method (yn and ynp1 only), ys analytical (yf only)
	real(fp) :: ys(2, 2)
	real(qp) :: ye(2)
	integer(kind=8) :: n

	!get command line args and then convert
	!https://gcc.gnu.org/onlinedocs/gfortran/GETARG.html
	!https://riptutorial.com/fortran/example/26615/passing-command-line-arguments
	call getarg(1, arg)
	read(arg, *)dt 
	!print *, dt

	!initial condition 
	y0(1) = 2.0_fp
	y0(2) = 0.0_fp

	if (sizeof(tf) == 16) then
		n = (tf - ti) / dt 
	else
		n = ceiling((tf - ti) / dt)
	end if
	
	call cpu_time(start)
	call n_4s3pB(ti, ys, n, dt, y0, a)
	call cpu_time(finish)
	print *, "method duration: ", finish-start
	
	!print *, "Solution final point: ", ys(1, 1), ys(1, 2)
	open(unit=2, file="../ref_solution/ref_sol.txt")
	read (2,*) ye(1), ye(2)
	print *, "method error: ", abs(ye(1) - real(ys(1, 1), qp))

!Why this contains statement? see last answer and comments of
!https://stackoverflow.com/questions/16741252/function-return-type-mismatch
!links to useful
!http://www.pcc.qub.ac.uk/tec/courses/f77tof90/stu-notes/f90studentMIF_4.html
contains

!Problem Functions
!y1 - variable 1
!y2 - variable 2
!t - current time
!a - problem constant
!f - return value
function f1_f(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: y1, y2, t, a 
	real(fp) :: val 
	val = y2 
end function f1_f

function f2_f(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: y1, y2, t, a 
	real(fp) :: val
	val = a * (1.0_fp - y1 * y1) * y2 - y1
end function f2_f

function f1_r(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: y1, y2, t, a 
	real(rp) :: val 
	val = y2 
end function f1_r

function f2_r(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: y1, y2, t, a 
	real(rp) :: val
	val = a * (1.0_rp - y1 * y1) * y2 - y1
end function f2_r

function mtranspose(m, n, a) result(b)
	use, intrinsic :: iso_fortran_env, only: rp=>REDU_TYPE, fp=>FULL_TYPE
	!fujitsu compilers do not have intrinsic transpose for real(2), hence our own
	implicit none 
	integer, value, intent(in) :: m, n 
	real(rp), intent(in) :: a(m, n)
	real(rp) :: b(n, m)
	integer :: i, j 

	do i=1,m 
		do j=1,n 
			b(j, i) = a(i, j)
		end do 
	end do 

end function mtranspose

subroutine inverse(a,c,n)
!============================================================
! Inverse matrix
! Method: Based on Doolittle LU factorization for Ax=b
! Alex G. December 2009
! shamelessly borrowed from https://ww2.odu.edu/~agodunov/computing/programs/book2/Ch06/Inverse.f90
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! n      - dimension
! output ...
! c(n,n) - inverse matrix of A
! comments ...
! the original matrix a(n,n) will be destroyed 
! during the calculation
!===========================================================
use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
implicit none 
integer n
real(rp) :: a(n,n), c(n,n)
real(rp) :: L(n,n), U(n,n), b(n), d(n), x(n)
real(rp) :: coeff
integer i, j, k

! step 0: initialization for matrices L and U and b
! Fortran 90/95 aloows such operations on matrices
L=0.0
U=0.0
b=0.0

! step 1: forward elimination
do k=1, n-1
   do i=k+1,n
      coeff=a(i,k)/a(k,k)
      L(i,k) = coeff
      do j=k+1,n
         a(i,j) = a(i,j)-coeff*a(k,j)
      end do
   end do
end do

! Step 2: prepare L and U matrices 
! L matrix is a matrix of the elimination coefficient
! + the diagonal elements are 1.0
do i=1,n
  L(i,i) = 1.0
end do
! U matrix is the upper triangular part of A
do j=1,n
  do i=1,j
    U(i,j) = a(i,j)
  end do
end do

! Step 3: compute columns of the inverse matrix C
do k=1,n
  b(k)=1.0
  d(1) = b(1)
! Step 3a: Solve Ld=b using the forward substitution
  do i=2,n
    d(i)=b(i)
    do j=1,i-1
      d(i) = d(i) - L(i,j)*d(j)
    end do
  end do
! Step 3b: Solve Ux=d using the back substitution
  x(n)=d(n)/U(n,n)
  do i = n-1,1,-1
    x(i) = d(i)
    do j=n,i+1,-1
      x(i)=x(i)-U(i,j)*x(j)
    end do
    x(i) = x(i)/u(i,i)
  end do
! Step 3c: fill the solutions x(n) into column k of C
  do i=1,n
    c(i,k) = x(i)
  end do
  b(k)=0.0
end do
end subroutine inverse

subroutine newtons(t, h, y, y0, a, fp_part, matAe, yi)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t, h, a 
	real(rp), intent(in) :: y(2), fp_part(2), matAe(4, 4)
	integer, intent(in) :: yi !which constant are we dealing with. Also the row for matAe
	real(rp), intent(inout) :: y0(4, 2)
	real(rp) :: tol, norm, sum_rp_part(2)
	real(rp) :: Fn(2), Fni(2), Jn(2, 2), Jn_(2, 2), Jni(2, 2)
	integer :: i, j, max_iter=20

	tol = epsilon(tol) * 1.001_rp

	do i=0, max_iter
		sum_rp_part(1) = 0.0_rp 
		sum_rp_part(2) = 0.0_rp 
		do j=1,yi
			sum_rp_part(1) = sum_rp_part(1) + matAe(yi, j) * f1_r(y0(j, 1), y0(j, 2), t, a)
			sum_rp_part(2) = sum_rp_part(2) + matAe(yi, j) * f2_r(y0(j, 1), y0(j, 2), t, a)
		end do

		Fn(1) = y(1) + fp_part(1) + h * sum_rp_part(1) - y0(yi, 1)
		Fn(2) = y(2) + fp_part(2) + h * sum_rp_part(2) - y0(yi, 2)
		norm = norm2(Fn) !norm2 is an intrinsic function
		if (norm < tol) then
			return !converged so can leave
		end if 

		Jn(1, 1) = -1.0_rp
		Jn(1, 2) = h / 2.0_rp
		Jn(2, 1) = 0.5_rp * h * (a * y0(yi, 1) * y0(yi, 2) - 1.0_rp)
		Jn(2, 2) = h / 2.0_rp * (a * (y0(yi, 1) * y0(yi, 1) - 1.0_rp)) - 1.0_rp
		call inverse(matmul(mtranspose(2, 2, Jn), Jn), Jni, 2) !transpose is an intrinsic function
		Jn_ = matmul(Jni, mtranspose(2, 2, Jn))
		Fni = matmul(Jn_, Fn)
		y0(yi, 1) = y0(yi, 1) - Fni(1)
		y0(yi, 2) = y0(yi, 2) - Fni(2)
	end do

end subroutine newtons

subroutine n_4s3pB(ti, ys, n, dt, y0, a)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=8), intent(in) :: n
	real(fp), intent(in) :: ti, y0(2)
	real(fp), intent(in) :: dt, a
	real(fp), intent(inout) :: ys(2, 2)
	integer(kind=8) :: t 
	real(rp) :: t_cur, fp_part(2)
	real(rp) :: yr(4, 2), y(2), matAe(4, 4)=0.0
	real(fp) :: ye(4, 2)
	real(fp) :: tmp, matA(4, 4)=0.0, b(4)=0.0!
	integer :: j

	!Butcher Coefficient assignment
	matA(2, 1) = 2.543016042796356_fp
	matA(3, 1) = 2.451484396921318_fp
	matA(3, 2) = 0.024108961241221_fp
	matA(4, 1) = 2.073861819468268_fp
	matA(4, 2) = 2.367724727682735_fp
	matA(4, 3) = 1.711868223075524_fp

	matAe(1, 1) = 0.5_rp
	matAe(2, 1) = -2.376349376129689_rp
	matAe(2, 2) = 0.5_rp
	matAe(3, 1) = -2.951484396921318_rp
	matAe(3, 2) = 0.475891038758779_rp
	matAe(3, 3) = 0.5_rp
	matAe(4, 1) = -0.573861819468268_rp
	matAe(4, 2) = -3.867724727682735_rp
	matAe(4, 3) = -1.211868223075524_rp
	matAe(4, 4) = 0.5_rp

	b(1) = 1.5_fp
	b(2) = -1.5_fp
	b(3) = 0.5_fp
	b(4) = 0.5_fp
	!end Butcher Coefficient assignment

	ys(1, 1) = y0(1)
	ys(1, 2) = y0(2)
	t_cur = real(ti, rp)

	do t=1,n
		t_cur = dt * t

		!initialize everything for newtons to the current point
		do j=1,4
			yr(j, 1) = real(ys(1, 1), rp) 
			yr(j, 2) = real(ys(1, 2), rp)
			ye(j, 1) = ys(1, 1) 
			ye(j, 2) = ys(1, 2)
		end do
		y(1) = real(ys(1, 1), rp)
		y(2) = real(ys(1, 2), rp)

		!stage1
		fp_part(1) = 0.0
		fp_part(2) = 0.0
		call newtons(t_cur, real(dt, rp), y, yr, real(a, rp), real(fp_part, rp), matAe, 1)
		ye(1, 1) = real(yr(1, 1), fp) !up cast
		ye(1, 2) = real(yr(1, 2), fp)
		!stage2
		fp_part(1) = 0.0
		fp_part(2) = 0.0
		do j=1,1
			fp_part(1) = fp_part(1) + dt * matA(2, j) * f1_f(ye(j, 1), ye(j, 2), dt * t, a)
			fp_part(2) = fp_part(2) + dt * matA(2, j) * f2_f(ye(j, 1), ye(j, 2), dt * t, a)
		end do
		call newtons(t_cur, real(dt, rp), y, yr, real(a, rp), real(fp_part, rp), matAe, 2)
		ye(2, 1) = real(yr(2, 1), fp) !up cast
		ye(2, 2) = real(yr(2, 2), fp)
		!stage3
		fp_part(1) = 0.0
		fp_part(2) = 0.0
		do j=1,2
			fp_part(1) = fp_part(1) + dt * matA(3, j) * f1_f(ye(j, 1), ye(j, 2), dt * t, a)
			fp_part(2) = fp_part(2) + dt * matA(3, j) * f2_f(ye(j, 1), ye(j, 2), dt * t, a)
		end do
		call newtons(t_cur, real(dt, rp), y, yr, real(a, rp), real(fp_part, rp), matAe, 3)
		ye(3, 1) = real(yr(3, 1), fp) !up cast
		ye(3, 2) = real(yr(3, 2), fp)
		!stage4
		fp_part(1) = 0.0
		fp_part(2) = 0.0
		do j=1,3
			fp_part(1) = fp_part(1) + dt * matA(4, j) * f1_f(ye(j, 1), ye(j, 2), dt * t, a)
			fp_part(2) = fp_part(2) + dt * matA(4, j) * f2_f(ye(j, 1), ye(j, 2), dt * t, a)
		end do
		call newtons(t_cur, real(dt, rp), y, yr, real(a, rp), real(fp_part, rp), matAe, 4)
		ye(4, 1) = real(yr(4, 1), fp) !up cast
		ye(4, 2) = real(yr(4, 2), fp)
		!update
		ys(2, 1) = ys(1, 1) 
		ys(2, 2) = ys(1, 2) 
		do j=1,4
			ys(2, 1) = ys(2, 1) + dt * b(j) * f1_f(ye(j, 1), ye(j, 2), dt * t, a)
			ys(2, 2) = ys(2, 2) + dt * b(j) * f2_f(ye(j, 1), ye(j, 2), dt * t, a)
		end do
		ys(1, 1) = ys(2, 1)
		ys(1, 2) = ys(2, 2)

	end do
	!print *, t_cur

end subroutine n_4s3pB

end program main