!gfortran sdirk-3p.f08 -o sdirk-3p -cpp -DFULL_TYPE=real128 -DREDU_TYPE=real32
! Full and reduced precision type defaults
#ifndef FULL_TYPE
#define FULL_TYPE real64
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real32
#endif

program main
	!Declare the full and reduced precision type names.
	!remember for literal constants to put 1.0_fp to cast correctly
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE, qp=>real128
	implicit none
	character(len=32) :: arg
	!dt, initial time, final time, a
	real(fp) :: dt, ti=0.0, tf=1.0, a=1.0
	real :: start, finish, corr=0.0 !start/finish times for method and correction step time used
	real(fp) :: y0(2) !initial condition
	!ys method (yn and ynp1 only), ys analytical (yf only)
	real(fp) :: ys(2, 2)
	real(qp) :: ye(2)
	integer(kind=8) :: n

	!get command line args and then convert
	!https://gcc.gnu.org/onlinedocs/gfortran/GETARG.html
	!https://riptutorial.com/fortran/example/26615/passing-command-line-arguments
	call getarg(1, arg)
	read(arg, *)dt 
	!print *, dt

	!initial condition 
	y0(1) = 2.0_fp
	y0(2) = 0.0_fp

	if (sizeof(tf) == 16) then
		n = (tf - ti) / dt 
	else
		n = ceiling((tf - ti) / dt)
	end if
	
	call cpu_time(start)
	call sdirk(ti, ys, n, dt, y0, a, corr)
	call cpu_time(finish)
	print *, "method duration: ", finish-start

	!print *, "Solution final point: ", ys(1, 1), ys(1, 2)
	open(unit=2, file="../ref_solution/ref_sol.txt")
	read (2,*) ye(1), ye(2)
	print *, "method error: ", abs(ye(1) - real(ys(1, 1), qp))
	!print *, "correction step time: ", corr


!Why this contains statement? see last answer and comments of
!https://stackoverflow.com/questions/16741252/function-return-type-mismatch
!links to useful
!http://www.pcc.qub.ac.uk/tec/courses/f77tof90/stu-notes/f90studentMIF_4.html
contains

!Problem Functions
!y1 - variable 1
!y2 - variable 2
!t - current time
!a - problem constant
!f - return value
function f1_f(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: y1, y2, t, a 
	real(fp) :: val 
	val = y2 
end function f1_f

function f2_f(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: y1, y2, t, a 
	real(fp) :: val
	val = a * (1.0_fp - y1 * y1) * y2 - y1 
end function f2_f

function f1_r(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: y1, y2, t, a 
	real(rp) :: val 
	val = y2 
end function f1_r

function f2_r(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: y1, y2, t, a 
	real(rp) :: val
	val = a * (1.0_rp - y1 * y1) * y2 - y1 
end function f2_r

function mtranspose(m, n, a) result(b)
	use, intrinsic :: iso_fortran_env, only: rp=>REDU_TYPE, fp=>FULL_TYPE
	!fujitsu compilers do not have intrinsic transpose for real(2), hence our own
	implicit none 
	integer, value, intent(in) :: m, n 
	real(rp), intent(in) :: a(m, n)
	real(rp) :: b(n, m)
	integer :: i, j 

	do i=1,m 
		do j=1,n 
			b(j, i) = a(i, j)
		end do 
	end do 

end function mtranspose

subroutine inverse(a,c,n)
!============================================================
! Inverse matrix
! Method: Based on Doolittle LU factorization for Ax=b
! Alex G. December 2009
! shamelessly borrowed from https://ww2.odu.edu/~agodunov/computing/programs/book2/Ch06/Inverse.f90
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! n      - dimension
! output ...
! c(n,n) - inverse matrix of A
! comments ...
! the original matrix a(n,n) will be destroyed 
! during the calculation
!===========================================================
use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
implicit none 
integer n
real(rp) :: a(n,n), c(n,n)
real(rp) :: L(n,n), U(n,n), b(n), d(n), x(n)
real(rp) :: coeff
integer i, j, k

! step 0: initialization for matrices L and U and b
! Fortran 90/95 aloows such operations on matrices
L=0.0
U=0.0
b=0.0

! step 1: forward elimination
do k=1, n-1
   do i=k+1,n
      coeff=a(i,k)/a(k,k)
      L(i,k) = coeff
      do j=k+1,n
         a(i,j) = a(i,j)-coeff*a(k,j)
      end do
   end do
end do

! Step 2: prepare L and U matrices 
! L matrix is a matrix of the elimination coefficient
! + the diagonal elements are 1.0
do i=1,n
  L(i,i) = 1.0
end do
! U matrix is the upper triangular part of A
do j=1,n
  do i=1,j
    U(i,j) = a(i,j)
  end do
end do

! Step 3: compute columns of the inverse matrix C
do k=1,n
  b(k)=1.0
  d(1) = b(1)
! Step 3a: Solve Ld=b using the forward substitution
  do i=2,n
    d(i)=b(i)
    do j=1,i-1
      d(i) = d(i) - L(i,j)*d(j)
    end do
  end do
! Step 3b: Solve Ux=d using the back substitution
  x(n)=d(n)/U(n,n)
  do i = n-1,1,-1
    x(i) = d(i)
    do j=n,i+1,-1
      x(i)=x(i)-U(i,j)*x(j)
    end do
    x(i) = x(i)/u(i,i)
  end do
! Step 3c: fill the solutions x(n) into column k of C
  do i=1,n
    c(i,k) = x(i)
  end do
  b(k)=0.0
end do
end subroutine inverse

subroutine newtons1(t, h, y, y0, a)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t, h, a 
	real(rp), intent(in) :: y(2)
	real(rp), intent(inout) :: y0(2)
	real(rp) :: tol, norm, gamma
	real(rp) :: Fn(2), Fni(2), Jn(2, 2), Jn_(2, 2), Jni(2, 2)
	integer :: i, max_iter=20

	tol = epsilon(tol) * 1.001_rp

#if REDU_TYPE == real16
	gamma = (1.73205_rp + 3.0_rp) / 6.0_rp
#else
	gamma = (sqrt(3.0_rp) + 3.0_rp) / 6.0_rp
#endif

	do i=0, max_iter-1
		Fn(1) = y(1) + gamma * h * f1_r(y0(1), y0(2), t, a) - y0(1)
		Fn(2) = y(2) + gamma * h * f2_r(y0(1), y0(2), t, a) - y0(2)
		norm = norm2(Fn) !norm2 is an intrinsic function
		if (norm < tol) then
			return !converged so can leave
		end if 

		Jn(1, 1) = -1.0_rp
		Jn(1, 2) = gamma * h 
		Jn(2, 1) = gamma * h * (a * y0(1) * y0(2) - 1.0_rp) 
		Jn(2, 2) = h * gamma * a * (1.0_rp - y0(1) * y0(1)) - 1.0_rp
		call inverse(matmul(mtranspose(2, 2, Jn), Jn), Jni, 2) !transpose is an intrinsic function
		Jn_ = matmul(Jni, mtranspose(2, 2, Jn))
		Fni = matmul(Jn_, Fn)
		y0(1) = y0(1) - Fni(1)
		y0(2) = y0(2) - Fni(2)
	end do

end subroutine newtons1

subroutine newtons2(t, h, y, y1, y0, a)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t, h, a 
	real(rp), intent(in) :: y(2), y1(2) !y is u_n, y1 is y^(1)
	real(rp), intent(inout) :: y0(2) !y0 is initial guess of y^(2). return as y^(2)
	real(rp) :: tol, norm, gamma
	real(rp) :: Fn(2), Fni(2), Jn(2, 2), Jn_(2, 2), Jni(2, 2)
	integer :: i, max_iter=20

	tol = epsilon(tol) + epsilon(tol) * 0.001

#if REDU_TYPE == real16
	gamma = (1.73205_rp + 3.0_rp) / 6.0_rp
#else
	gamma = (sqrt(3.0_rp) + 3.0_rp) / 6.0_rp
#endif

	do i=0, max_iter-1
		Fn(1) = y(1) + (1.0_rp - 2.0_rp * gamma) * h * f1_r(y1(1), y1(2), t, a) &
			+ gamma * h * f1_r(y0(1), y0(2), t, a) - y0(1)
		Fn(2) = y(2) + (1.0_rp - 2.0_rp * gamma) * h * f2_r(y1(1), y1(2), t, a) & 
			+ gamma * h * f2_r(y0(1), y0(2), t, a) - y0(2)
		norm = norm2(Fn) !norm2 is an intrinsic function
		if (norm < tol) then
			return !converged so can leave
		end if 

		Jn(1, 1) = -1.0_rp
		Jn(1, 2) = gamma * h 
		Jn(2, 1) = gamma * h * (a * y0(1) * y0(2) - 1.0_rp) 
		Jn(2, 2) = h * gamma * a * (1.0_rp - y0(1) * y0(1)) - 1.0_rp
		call inverse(matmul(mtranspose(2, 2, Jn), Jn), Jni, 2) !transpose is an intrinsic function
		Jn_ = matmul(Jni, mtranspose(2, 2, Jn))
		Fni = matmul(Jn_, Fn)
		y0(1) = y0(1) - Fni(1)
		y0(2) = y0(2) - Fni(2)
	end do

end subroutine newtons2

subroutine correction1(ys, ye, gamma, dt, t, a, n_correct)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: ys(2, 2), gamma, dt, a 
	real(fp), intent(inout) :: ye(2)
	integer(kind=8), intent(in) :: t, n_correct 
	integer(kind=8) :: k

	if (n_correct == 0) then
		return
	endif

	do k=1,n_correct
		ye(1) = ys(1, 1) + gamma * dt * f1_f(ye(1), ye(2), dt * t, a)
		ye(2) = ys(1, 2) + gamma * dt * f2_f(ye(1), ye(2), dt * t, a)
	end do

end subroutine correction1

subroutine correction2(ys, yei, yeii, gamma, dt, t, a, n_correct)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: ys(2, 2), yei(2), gamma, dt, a 
	real(fp), intent(inout) :: yeii(2)
	real(fp) :: part1, part2
	integer(kind=8), intent(in) :: t, n_correct 
	integer(kind=8) :: k

	if (n_correct == 0) then
		return
	endif

	!only need to calculate this once so pull out this part as a constant
	part1 = (1.0_fp - 2.0_fp * gamma) * dt * f1_f(yei(1), yei(2), dt * t, a)
	part2 = (1.0_fp - 2.0_fp * gamma) * dt * f2_f(yei(1), yei(2), dt * t, a)
	do k=1,n_correct
		yeii(1) = ys(1, 1) + part1 + gamma * dt * f1_f(yeii(1), yeii(2), dt * t, a)
		yeii(2) = ys(1, 2) + part2 + gamma * dt * f2_f(yeii(1), yeii(2), dt * t, a)
	end do

end subroutine correction2

subroutine sdirk(ti, ys, n, dt, y0, a, corr)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=8), intent(in) :: n
	real(fp), intent(in) :: ti, y0(2)
	real(fp), intent(in) :: dt, a
	real(fp), intent(inout) :: ys(2, 2) !ys(1, *) serves as un for ys(2, *) which is un+1
	real, intent(inout) :: corr
	integer(kind=8) :: t, k, n_correct
	real(rp) :: t_cur
	real(rp) :: yi(2), yii(2), y(2)
	real(fp) :: yei(2), yeii(2), gamma
	real :: start, finish

	ys(1, 1) = y0(1)
	ys(1, 2) = y0(2)
	t_cur = real(ti, rp)
	if (sizeof(gamma) == sizeof(t_cur)) then
		n_correct = 0 !no correction when full and reduced precisions are same
	else
		n_correct = 3
	endif

#if FULL_TYPE == real16
	gamma = (1.73205_fp + 3.0_fp) / 6.0_fp
#else
	gamma = (sqrt(3.0_fp) + 3.0_fp) / 6.0_fp
#endif

	do t=1,n
		yi(1) = real(ys(1, 1), rp) !convert from ys full precision to reduced
		yi(2) = real(ys(1, 2), rp) !https://stackoverflow.com/questions/26930536/fortran-cast-double-precision-complex-to-single-precision
		yii(1) = real(ys(1, 1), rp) 
		yii(2) = real(ys(1, 2), rp)
		y(1) = real(ys(1, 1), rp)
		y(2) = real(ys(1, 2), rp)

		t_cur = real(dt * t, rp) !current time
		!stage 1 implicit step
		call newtons1(t_cur, real(dt, rp), y, yi, real(a, rp))
		!stage 1 correction step
		yei(1) = real(yi(1), fp) !up cast
		yei(2) = real(yi(2), fp)
		!call cpu_time(start)
		call correction1(ys, yei, gamma, dt, t, a, n_correct)
		!call cpu_time(finish)
		!corr = corr + (finish-start)
		!stage 2 implicit step
		call newtons2(t_cur, real(dt, rp), y, real(yei, rp), yii, real(a, rp))
		!stage 2 correction step
		yeii(1) = real(yii(1), fp) !up cast
		yeii(2) = real(yii(2), fp)
		!call cpu_time(start)
		call correction2(ys, yei, yeii, gamma, dt, t, a, n_correct)	
		!call cpu_time(finish)
		!corr = corr + (finish-start)
		!update stage
		ys(2, 1) = ys(1, 1) + 0.5_fp * dt * f1_f(yei(1), yei(2), dt * t, a) &
			+ 0.5_fp * dt * f1_f(yeii(1), yeii(2), dt * t, a)
		ys(2, 2) = ys(1, 2) + 0.5_fp * dt * f2_f(yei(1), yei(2), dt * t, a) &
			+ 0.5_fp * dt * f2_f(yeii(1), yeii(2), dt * t, a)
		ys(1, 1) = ys(2, 1)
		ys(1, 2) = ys(2, 2)

	end do
	!print *, t_cur

end subroutine sdirk

end program main