import json
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.cm as cm
import argparse as ap 
import os 

def plot_test_list(test, dts, plot_en=False, save=False, save_type="png"):
	is_corr = False
	if ("novel" not in test) and (("1p" in test) or ("2p" in test) or ("3p" in test)):
		is_corr = True 
	if plot_en:
		with open(f'./energy_exp_{test}.json', 'r') as f:
			runs = json.load(f)
		if is_corr:
			with open(f'./energy_exp_{test[:-3]}.json', 'r') as f:
				runs_nocorr = json.load(f)
	else:
		with open(f'./{test}/runs_aarch64_{test}.json', 'r') as f:
			runs = json.load(f)
		if is_corr:
			with open(f'./{test[:-3]}/runs_aarch64_{test[:-3]}.json', 'r') as f:
				runs_nocorr = json.load(f)

	if not os.path.exists(f'./images/{test}'):
		os.makedirs(f'./images/{test}')

	colors = cm.turbo(np.linspace(0, 1, len(runs))) #https://matplotlib.org/stable/tutorials/colors/colormaps.html
	for i, run in enumerate(runs):
		if is_corr and (run['full'] == run['redu']):
			plt.loglog(dts, runs_nocorr[i]['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(dts, runs_nocorr[i]['errs'], color=colors[i], s=(len(runs)-i)*20)
		else:
			plt.loglog(dts, run['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(dts, run['errs'], color=colors[i], s=(len(runs)-i)*20)

	#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
	plt.xlabel('dt')
	plt.ylabel('error')
	#plt.ylim(top=1e-3)
	plt.xticks(dts)
	plt.grid()
	#plt.ylim(top=1e0)
	plt.legend()
	if not save:
		plt.show()
	else:
		plt.savefig(f'./images/{test}/err_v_dt.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	for i, run in enumerate(runs):
		if is_corr and (run['full'] == run['redu']):
			plt.loglog(dts, runs_nocorr[i]['ts'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(dts, runs_nocorr[i]['ts'], color=colors[i], s=(len(runs)-i)*20)
		else:
			plt.loglog(dts, run['ts'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(dts, run['ts'], color=colors[i], s=(len(runs)-i)*20)

	#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
	plt.xlabel('dt')
	plt.ylabel('Runtime (s)')
	plt.legend()
	plt.grid()
	plt.yticks([10**(i) for i in range(-4, 5)])
	plt.xticks(dts)
	plt.ylim(top=1e4)
	if not save:
		plt.show()
	else:
		plt.savefig(f'./images/{test}/ts_v_dt.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	for i, run in enumerate(runs):
		if is_corr and (run['full'] == run['redu']):
			plt.loglog(runs_nocorr[i]['ts'], runs_nocorr[i]['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(runs_nocorr[i]['ts'], runs_nocorr[i]['errs'], color=colors[i], s=(len(runs)-i)*20)
		else:
			plt.loglog(run['ts'], run['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(run['ts'], run['errs'], color=colors[i], s=(len(runs)-i)*20)
	plt.title(f"Mixed:{test.upper()}\nNo-Mix:{test[:-3].upper()}")
	plt.xlabel('Runtime (s)')
	plt.ylabel('Error')
	#plt.xlim(right=1e-1, left=1e-7)
	plt.legend()
	plt.grid()
	if not save:
		plt.show()
	else:
		plt.savefig(f'./images/{test}/ts_v_err.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	if plot_en:
		for i, run in enumerate(runs):
			if is_corr and (run['full'] == run['redu']):
				plt.loglog(dts, runs_nocorr[i]['ener'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(dts, runs_nocorr[i]['ener'], color=colors[i], s=(len(runs)-i)*20)
			else:
				plt.loglog(dts, run['ener'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(dts, run['ener'], color=colors[i], s=(len(runs)-i)*20)

		#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
		plt.xlabel('dt')
		plt.ylabel('Energy (J)')
		plt.xticks(dts)
		#plt.ylim(top=1e0)
		#plt.ylim(bottom=14)
		plt.legend()
		plt.grid()
		if not save:
			plt.show()
		else:
			plt.savefig(f'./images/{test}/en_v_dt.{save_type}', format=f'{save_type}', dpi=1000)
			plt.clf()

		for i, run in enumerate(runs):
			if is_corr and (run['full'] == run['redu']):
				plt.loglog(runs_nocorr[i]['ener'], runs_nocorr[i]['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(runs_nocorr[i]['ener'], runs_nocorr[i]['errs'], color=colors[i], s=(len(runs)-i)*20)
			else:
				plt.loglog(run['ener'], run['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(run['ener'], run['errs'], color=colors[i], s=(len(runs)-i)*20)
		#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
		plt.xlabel('Energy (J)')
		plt.ylabel('Error')
		#plt.xticks(dts)
		#plt.ylim(top=1e0)
		#plt.ylim(bottom=14)
		plt.legend()
		plt.grid()
		if not save:
			plt.show()
		else:
			plt.savefig(f'./images/{test}/en_v_err.{save_type}', format=f'{save_type}', dpi=1000)
			plt.clf()

		"""
		for i, run in enumerate(runs):
			#if i == 1 or i == 2 or i == 0:
			#	continue
			money = np.array(run['ener']) / 3.6e+6 * 22.11
			plt.loglog(money, run['errs'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(money, run['errs'], color=colors[i], s=(len(runs)-i)*20)
		#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
		plt.xlabel('Money (cents)')
		plt.ylabel('Error')
		plt.xlim(right=1e-1, left=1e-7)
		plt.legend()
		plt.grid()
		if not save:
			plt.show()
		else:
			plt.savefig(f'./images/{test}/cents_v_err.{save_type}', format=f'{save_type}', dpi=1000)
			plt.clf()
		"""

		for i, run in enumerate(runs):
			if is_corr and (run['full'] == run['redu']):
				plt.loglog(runs_nocorr[i]['ts'], runs_nocorr[i]['ener'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(runs_nocorr[i]['ts'], runs_nocorr[i]['ener'], color=colors[i], s=(len(runs)-i)*20)
			else:
				plt.loglog(run['ts'], run['ener'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
				plt.scatter(run['ts'], run['ener'], color=colors[i], s=(len(runs)-i)*20)
		#plt.title(f"Method: {test.upper()} for the\nVan der Pol T=[0,1]")
		plt.ylabel('Energy (J)')
		plt.xlabel('Runtime (s)')
		#plt.xticks(dts)
		#plt.ylim(top=1e0)
		#plt.ylim(bottom=14)
		plt.legend()
		plt.grid()
		if not save:
			plt.show()
		else:
			plt.savefig(f'./images/{test}/en_v_ts.{save_type}', format=f'{save_type}', dpi=1000)
			plt.clf()


def compare_tests_for_precision(tests, dts, fp, rp):
	colors = cm.turbo(np.linspace(0, 1, len(tests)))
	for j, test in enumerate(tests):
		with open(f'./{test}/runs_aarch64_{test}.json', 'r') as f:
			runs = json.load(f)
		
		for i, run in enumerate(runs):
			if run["full"] == fp and run["redu"] == rp:
				plt.loglog(dts, run['errs'], label=f'{test}', color=colors[j])
				plt.scatter(dts, run['errs'], color=colors[j], s=(len(tests)-j)*20)
	plt.title(f"Van der Pol T=[0,0.5] With Precision:\nFull: {fp}, Reduced: {rp}")
	plt.xlabel('dt')
	plt.ylabel('error')
	#plt.ylim(top=1e0)
	plt.legend()
	plt.show()

	for j, test in enumerate(tests):
		with open(f'./{test}/runs_aarch64_{test}.json', 'r') as f:
			runs = json.load(f)
		
		for i, run in enumerate(runs):
			if run["full"] == fp and run["redu"] == rp:
				plt.loglog(dts, run['ts'], label=f'{test}', color=colors[j])
				plt.scatter(dts, run['ts'], color=colors[j], s=(len(tests)-j)*20)
	plt.title(f"Van der Pol T=[0,1] With Precision:\nFull: {fp}, Reduced: {rp}")
	plt.xlabel('dt')
	plt.ylabel('Runtime (s)')
	plt.legend()
	plt.grid()
	plt.yticks([10**(i) for i in range(-4, 5)])
	plt.xticks(dts)
	plt.ylim(top=1e4)
	plt.show()
	#plt.savefig(f'./images/{test}/{fp}_vs_{rp}.{save_type}', format=f'{save_type}', dpi=1000)

def test_difference_of_methods(testa, testb):
	with open(f'./{testa}/runs_aarch64_{testa}.json', 'r') as f:
		runsA = json.load(f) 
	with open(f'./{testb}/runs_aarch64_{testb}.json', 'r') as f:
		runsB = json.load(f)

	for i, runz in enumerate(zip(runsA, runsB)):
		runa, runb = runz
		print(f'Precision: fp:{runa["full"]}, rp:{runa["redu"]}')
		print(np.array(runa['errs']) - np.array(runb['errs']))
		print(np.array(runa['ts']) - np.array(runb['ts']))

def main():
	parser = ap.ArgumentParser(description='Plot the results of the tests!')
	parser.add_argument('test', metavar='T', type=str, help='Name of the test to plot (dir name)')
	parser.add_argument('--save', action='store_true', help='If set saves plots else display')
	args = parser.parse_args()
	dts = [1*10**(-i) for i in range(1, 8)]
	plot_test_list(args.test, dts, plot_en=False, save=args.save)
	#compare_tests_for_precision(tests, dts, 'real128', 'real128')
	#test_difference_of_methods('impmid', 'eff-impmid')

if __name__ == '__main__':
	main()