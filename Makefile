CC=gcc
CXX=g++
PAPILIB := -L/$(PAPI_DIR)/lib -lpapi
PAPIINC := -I/$(PAPI_DIR)/include

run_powercap: run_powercap.c
	$(CC) -Wall -o run_powercap.x run_powercap.c $(PAPILIB) $(PAPIINC)
