!gfortran sdirk.f08 -o sdirk -cpp -DFULL_TYPE=real128 -DREDU_TYPE=real32
! Full and reduced precision type defaults
#ifndef FULL_TYPE
#define FULL_TYPE real64
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real32
#endif

program main
	!Declare the full and reduced precision type names.
	!remember for literal constants to put 1.0_fp to cast correctly
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	character(len=32) :: arg
	!dt, initial time, final time, a
	real(fp) :: dt, ti=0.0, tf=1.0, a=1.0
	real :: start, finish !start/finish times for method
	real(fp) :: y0(2) !initial condition
	!ys method (yn and ynp1 only), ys analytical (yf only)
	real(fp) :: ys(2, 2)
	integer(kind=8) :: n

	!get command line args and then convert
	!https://gcc.gnu.org/onlinedocs/gfortran/GETARG.html
	!https://riptutorial.com/fortran/example/26615/passing-command-line-arguments
	call getarg(1, arg)
	read(arg, *)dt 
	!print *, dt

	!initial condition 
	y0(1) = 2.0_fp
	y0(2) = 0.0_fp

	n = (tf - ti) / dt
	
	call cpu_time(start)
	!call lobatto(ti, ys, n, dt, y0, a)
	call rk4(ti, tf, ys, dt, y0, a)
	call cpu_time(finish)
	print *, "method duration: ", finish-start


!Why this contains statement? see last answer and comments of
!https://stackoverflow.com/questions/16741252/function-return-type-mismatch
!links to useful
!http://www.pcc.qub.ac.uk/tec/courses/f77tof90/stu-notes/f90studentMIF_4.html
contains

!Problem Functions
!y1 - variable 1
!y2 - variable 2
!t - current time
!a - problem constant
!f - return value
function f1_f(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: y1, y2, t, a 
	real(fp) :: val 
	val = y2 
end function f1_f

function f2_f(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: y1, y2, t, a 
	real(fp) :: val
	val = a * (1.0_fp - y1 * y1) * y2 - y1
end function f2_f

function f1_r(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: y1, y2, t, a 
	real(rp) :: val 
	val = y2 
end function f1_r

function f2_r(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: y1, y2, t, a 
	real(rp) :: val
	val = a * (1.0_rp - y1 * y1) * y2 - y1
end function f2_r

subroutine inverse(a,c,n)
!============================================================
! Inverse matrix
! Method: Based on Doolittle LU factorization for Ax=b
! Alex G. December 2009
! shamelessly borrowed from https://ww2.odu.edu/~agodunov/computing/programs/book2/Ch06/Inverse.f90
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! n      - dimension
! output ...
! c(n,n) - inverse matrix of A
! comments ...
! the original matrix a(n,n) will be destroyed 
! during the calculation
!===========================================================
use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
implicit none 
integer n
real(rp) :: a(n,n), c(n,n)
real(rp) :: L(n,n), U(n,n), b(n), d(n), x(n)
real(rp) :: coeff
integer i, j, k

! step 0: initialization for matrices L and U and b
! Fortran 90/95 aloows such operations on matrices
L=0.0
U=0.0
b=0.0

! step 1: forward elimination
do k=1, n-1
   do i=k+1,n
      coeff=a(i,k)/a(k,k)
      L(i,k) = coeff
      do j=k+1,n
         a(i,j) = a(i,j)-coeff*a(k,j)
      end do
   end do
end do

! Step 2: prepare L and U matrices 
! L matrix is a matrix of the elimination coefficient
! + the diagonal elements are 1.0
do i=1,n
  L(i,i) = 1.0
end do
! U matrix is the upper triangular part of A
do j=1,n
  do i=1,j
    U(i,j) = a(i,j)
  end do
end do

! Step 3: compute columns of the inverse matrix C
do k=1,n
  b(k)=1.0
  d(1) = b(1)
! Step 3a: Solve Ld=b using the forward substitution
  do i=2,n
    d(i)=b(i)
    do j=1,i-1
      d(i) = d(i) - L(i,j)*d(j)
    end do
  end do
! Step 3b: Solve Ux=d using the back substitution
  x(n)=d(n)/U(n,n)
  do i = n-1,1,-1
    x(i) = d(i)
    do j=n,i+1,-1
      x(i)=x(i)-U(i,j)*x(j)
    end do
    x(i) = x(i)/u(i,i)
  end do
! Step 3c: fill the solutions x(n) into column k of C
  do i=1,n
    c(i,k) = x(i)
  end do
  b(k)=0.0
end do
end subroutine inverse

subroutine newtons1(t, h, y, y0, a, i)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t, h, a 
	real(rp), intent(in) :: y(2)
	real(rp), intent(inout) :: y0(2)
	integer, intent(inout) :: i
	real(rp) :: tol, norm, gamma
	real(rp) :: Fn(2), Fni(2), Jn(2, 2), Jn_(2, 2), Jni(2, 2)
	integer :: max_iter=20

	tol = epsilon(tol) + epsilon(tol) * 0.001_rp

	gamma = (sqrt(3.0_rp) + 3.0_rp) / 6.0_rp

	do i=0, max_iter-1
		Fn(1) = y(1) + gamma * h * f1_r(y0(1), y0(2), t, a) - y0(1)
		Fn(2) = y(2) + gamma * h * f2_r(y0(1), y0(2), t, a) - y0(2)
		norm = norm2(Fn) !norm2 is an intrinsic function
		if (norm < tol) then
			return !converged so can leave
		end if 

		Jn(1, 1) = -1.0_rp
		Jn(1, 2) = gamma * h 
		Jn(2, 1) = gamma * h * (a * y0(1) * y0(2) - 1.0_rp) 
		Jn(2, 2) = h * gamma * a * (1.0_rp - y0(1) * y0(1)) - 1.0_rp
		call inverse(matmul(transpose(Jn), Jn), Jni, 2) !transpose is an intrinsic function
		Jn_ = matmul(Jni, Jn)
		Fni = matmul(Jn_, Fn)
		y0(1) = y0(1) - Fni(1)
		y0(2) = y0(2) - Fni(2)
	end do

end subroutine newtons1

subroutine newtons2(t, h, y, y1, y0, a, i)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t, h, a 
	real(rp), intent(in) :: y(2), y1(2) !y is u_n, y1 is y^(1)
	real(rp), intent(inout) :: y0(2) !y0 is initial guess of y^(2). return as y^(2)
	integer, intent(inout) :: i
	real(rp) :: tol, norm, gamma
	real(rp) :: Fn(2), Fni(2), Jn(2, 2), Jn_(2, 2), Jni(2, 2)
	integer :: max_iter=20

	tol = epsilon(tol) + epsilon(tol) * 0.001_rp

	gamma = (sqrt(3.0_rp) + 3.0_rp) / 6.0_rp

	do i=0, max_iter-1
		Fn(1) = y(1) + (1.0_rp - 2.0_rp * gamma) * h * f1_r(y1(1), y1(2), t, a) &
			+ gamma * h * f1_r(y0(1), y0(2), t, a) - y0(1)
		Fn(2) = y(2) + (1.0_rp - 2.0_rp * gamma) * h * f2_r(y1(1), y1(2), t, a) & 
			+ gamma * h * f2_r(y0(1), y0(2), t, a) - y0(2)
		norm = norm2(Fn) !norm2 is an intrinsic function
		if (norm < tol) then
			return !converged so can leave
		end if 

		Jn(1, 1) = -1.0_rp
		Jn(1, 2) = gamma * h 
		Jn(2, 1) = gamma * h * (a * y0(1) * y0(2) - 1.0_rp) 
		Jn(2, 2) = h * gamma * a * (1.0_rp - y0(1) * y0(1)) - 1.0_rp
		call inverse(matmul(transpose(Jn), Jn), Jni, 2) !transpose is an intrinsic function
		Jn_ = matmul(Jni, Jn)
		Fni = matmul(Jn_, Fn)
		y0(1) = y0(1) - Fni(1)
		y0(2) = y0(2) - Fni(2)
	end do

end subroutine newtons2

subroutine correction1(ys, ye, gamma, dt, t, a, n_correct)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: ys(2, 2), gamma, dt, a 
	real(fp), intent(inout) :: ye(2)
	integer, intent(in) :: t, n_correct 
	integer :: k

	do k=1,n_correct
		ye(1) = ys(1, 1) + gamma * dt * f1_f(ye(1), ye(2), dt * t, a)
		ye(2) = ys(1, 2) + gamma * dt * f2_f(ye(1), ye(2), dt * t, a)
	end do

end subroutine correction1

subroutine correction2(ys, yei, yeii, gamma, dt, t, a, n_correct)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: ys(2, 2), yei(2), gamma, dt, a 
	real(fp), intent(inout) :: yeii(2)
	real(fp) :: part1, part2
	integer, intent(in) :: t, n_correct 
	integer :: k

	part1 = (1.0_fp + 2.0_fp * gamma) * dt * f1_f(yei(1), yei(1), dt * t, a)
	part2 = (1.0_fp + 2.0_fp * gamma) * dt * f2_f(yei(1), yei(1), dt * t, a)
	do k=1,n_correct
		yeii(1) = ys(1, 1) + part1 + gamma * dt * f1_f(yeii(1), yeii(2), dt * t, a)
		yeii(2) = ys(1, 2) + part2 + gamma * dt * f2_f(yeii(1), yeii(2), dt * t, a)
	end do

end subroutine correction2

subroutine sdirk(ti, ys, n, dt, y0, a)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer, intent(in) :: n
	real(fp), intent(in) :: ti, y0(2)
	real(fp), intent(in) :: dt, a
	real(fp), intent(inout) :: ys(2, 2) !ys(1, *) serves as un for ys(2, *) which is un+1
	integer :: t, k, i_new1, i_new2, i_new1_tot=0, i_new2_tot=0
	real(rp) :: yi(2), yii(2), y(2)
	real(fp) :: yei(2), yeii(2), gamma

	ys(1, 1) = y0(1)
	ys(1, 2) = y0(2)

	gamma = (sqrt(3.0_fp) + 3.0_fp) / 6.0_fp

	open(unit=2, file='ref_sol.txt', ACTION="write", STATUS="replace")
	open(unit=12, file='ref_sol_t.txt', ACTION="write", STATUS="replace")
	write(12,*) (dt * t)
	do t=1,n
		write(2,*) ys(1, :)
		yi(1) = real(ys(1, 1), rp) !convert from ys full precision to reduced
		yi(2) = real(ys(1, 2), rp) !https://stackoverflow.com/questions/26930536/fortran-cast-double-precision-complex-to-single-precision
		yii(1) = real(ys(1, 1), rp) 
		yii(2) = real(ys(1, 2), rp)
		y(1) = real(ys(1, 1), rp)
		y(2) = real(ys(1, 2), rp)

		write(12,*) (dt * t)
		!stage 1 implicit
		call newtons1(real(dt * t, rp), real(dt, rp), y, yi, real(a, rp), i_new1)
		!stage 1 correction
		yei(1) = real(yi(1), fp) !up cast
		yei(2) = real(yi(2), fp)
		call correction1(ys, yei, gamma, dt, t, a, 3)
		!stage 2 implicit
		call newtons2(real(dt * t, rp), real(dt, rp), y, yi, yii, real(a, rp), i_new2)
		!stage 2 correction
		yeii(1) = real(yii(1), fp)
		yeii(2) = real(yii(2), fp)
		call correction2(ys, yei, yeii, gamma, dt, t, a, 3)
		!stage 3 update
		ys(2, 1) = ys(1, 1) + 0.5_fp * dt * f1_f(yei(1), yei(2), dt * t, a) &
			+ 0.5_fp * dt * f1_f(yeii(1), yeii(2), dt * t, a)
		ys(2, 2) = ys(1, 2) + 0.5_fp * dt * f2_f(yei(1), yei(2), dt * t, a) &
			+ 0.5_fp * dt * f2_f(yeii(1), yeii(2), dt * t, a)
		ys(1, 1) = ys(2, 1)
		ys(1, 2) = ys(2, 2)
		!update average newton iterations
		i_new1_tot = i_new1_tot + i_new1 
		i_new2_tot = i_new2_tot + i_new2 

	end do
	print *, "Average newton1 iterations: ", i_new1_tot/n 
	print *, "Average newton2 iterations: ", i_new2_tot/n 
	write(2,*) ys(1, :)
	close(2)
	close(12)

end subroutine sdirk

!! START LOBATTO STUFF
subroutine jacobian(x1, x2, y1, y2, a, dt, J)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(rp), intent(in) :: x1, x2, y1, y2, a, dt 
	real(rp), intent(inout) :: J(8, 4)

	J(1, 1) = -1.0_rp 
	J(1, 2) = 0.5_rp * dt 
	J(1, 3) = 0.0_rp 
	J(1, 4) = -0.5_rp * dt 
	J(2, 1) = 0.0_rp 
	J(2, 2) = 0.5_rp * dt - 1.0_rp 
	J(2, 3) = 0.0_rp 
	J(2, 4) = -0.5_rp * dt 
	J(3, 1) = 0.0_rp
	J(3, 2) = 0.5_rp * dt 
	J(3, 3) = -1.0_rp 
	J(3, 4) = 0.5_rp * dt 
	J(4, 1) = 0.0_rp 
	J(4, 2) = 0.5_rp * dt 
	J(4, 3) = 0.0_rp 
	J(4, 4) = 0.5_rp * dt - 1.0_rp 

	J(5, 1) = 0.5_rp * dt * (2.0_rp * a * x1 * x2 - 1.0_rp) - 1.0_rp 
	J(5, 2) = 0.5_rp * a * dt * (1.0_rp - x1 * x1)
	J(5, 3) = -0.5_rp * dt * (2.0_rp * a * y1 * y2 - 1.0_rp)
	J(5, 4) = -0.5_rp * a * dt * (1.0_rp - y1 * y1)
	J(6, 1) = 0.5_rp * dt * (2.0_rp * a * x1 * x2 - 1.0_rp) 
	J(6, 2) = 0.5_rp * a * dt * (1.0_rp - x1 * x1) - 1.0_rp
	J(6, 3) = -0.5_rp * dt * (2.0_rp * a * y1 * y2 - 1.0_rp) 
	J(6, 4) = -0.5_rp * a * dt * (1.0_rp - y1 * y1)
	J(7, 1) = 0.5_rp * dt * (2.0_rp * a * x1 * x2 - 1.0_rp) 
	J(7, 2) = 0.5_rp * a * dt * (1.0_rp - x1 * x1)
	J(7, 3) = 0.5_rp * dt * (2.0_rp * a * y1 * y2 - 1.0_rp) - 1.0_rp 
	J(7, 4) = 0.5_rp * a * dt * (1.0_rp - y1 * y1)
	J(8, 1) = 0.5_rp * dt * (2.0_rp * a * x1 * x2 - 1.0_rp)
	J(8, 2) = 0.5_rp * a * dt * (1.0_rp - x1 * x1)
	J(8, 3) = 0.5_rp * dt * (2.0_rp * a * y1 * y2 - 1.0_rp) 
	J(8, 4) = 0.5_rp * a * dt * (1.0_rp - y1 * y1) - 1.0_rp
end subroutine jacobian

subroutine newtons_l(t, h, y, x0, y0, a, i)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t, h, a 
	real(rp), intent(in) :: y(2)
	real(rp), intent(inout) :: x0(2), y0(2)
	integer(kind=8), intent(inout) :: i
	real(rp) :: tol, norm
	real(rp) :: Fn(8), Fni(4), Jn(8, 4), Jn_(4, 8), Jni(4, 4)
	integer :: max_iter=20

	tol = epsilon(tol) + epsilon(tol) * 0.001_rp
	!print *, "tol: ", tol

	do i=0, max_iter-1
		Fn(1) = y(1) + h/2.0_rp * f1_r(x0(1), x0(2), t, a) - h/2.0_rp * f1_r(y0(1), y0(2), t, a) - x0(1)
		Fn(2) = y(2) + h/2.0_rp * f1_r(x0(1), x0(2), t, a) - h/2.0_rp * f1_r(y0(1), y0(2), t, a) - x0(2)
		Fn(3) = y(1) + h/2.0_rp * f1_r(x0(1), x0(2), t, a) + h/2.0_rp * f1_r(y0(1), y0(2), t, a) - y0(1)
		Fn(4) = y(2) + h/2.0_rp * f1_r(x0(1), x0(2), t, a) + h/2.0_rp * f1_r(y0(1), y0(2), t, a) - y0(2)
		Fn(5) = y(1) + h/2.0_rp * f2_r(x0(1), x0(2), t, a) - h/2.0_rp * f2_r(y0(1), y0(2), t, a) - x0(1)
		Fn(6) = y(2) + h/2.0_rp * f2_r(x0(1), x0(2), t, a) - h/2.0_rp * f2_r(y0(1), y0(2), t, a) - x0(2)
		Fn(7) = y(1) + h/2.0_rp * f2_r(x0(1), x0(2), t, a) + h/2.0_rp * f2_r(y0(1), y0(2), t, a) - y0(1)
		Fn(8) = y(2) + h/2.0_rp * f2_r(x0(1), x0(2), t, a) + h/2.0_rp * f2_r(y0(1), y0(2), t, a) - y0(2)
		norm = norm2(Fn) !norm2 is an intrinsic function
		!print *, "norm: ", norm
		if (norm < tol) then
			return !converged so can leave
		end if 

		call jacobian(x0(1), x0(2), y0(1), y0(2), a, h, Jn)
		call inverse(matmul(transpose(Jn), Jn), Jni, 4) !transpose is an intrinsic function
		Jn_ = matmul(Jni, transpose(Jn))
		Fni = matmul(Jn_, Fn)
		x0(1) = x0(1) - Fni(1)
		x0(2) = x0(2) - Fni(2)
		y0(1) = y0(1) - Fni(3)
		y0(2) = y0(2) - Fni(4)
	end do

end subroutine newtons_l

subroutine lobatto(ti, ys, n, dt, y0, a)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=8), intent(in) :: n
	real(fp), intent(in) :: ti, y0(2)
	real(fp), intent(in) :: dt, a
	real(fp), intent(inout) :: ys(2, 2)
	integer(kind=8) :: t, i_new, i_new_tot=0
	real(rp) :: yi(2), yii(2), y(2)
	real(fp) :: yei(2), yeii(2)

	ys(1, 1) = y0(1)
	ys(1, 2) = y0(2)

	open(unit=2, file='ref_sol.txt', ACTION="write", STATUS="replace")
	open(unit=12, file='ref_sol_t.txt', ACTION="write", STATUS="replace")
	write(12,*) (dt * t)
	do t=1,n
		write(2,*) ys(1, :)
		yi(1) = real(ys(1, 1), rp)
		yi(2) = real(ys(1, 2), rp)
		yii(1) = real(ys(1, 1), rp)
		yii(2) = real(ys(1, 2), rp)
		y(1) = real(ys(1, 1), rp)
		y(2) = real(ys(1, 2), rp)

		write(12,*) (dt * t)
		!print *, "enter newtons"
		call newtons_l(real(dt * t, rp), real(dt, rp), y, yi, yii, real(a, rp), i_new)
		i_new_tot = i_new_tot + i_new 
		yei(1) = real(yi(1), fp) !up cast
		yei(2) = real(yi(2), fp)
		yeii(1) = real(yii(1), fp)
		yeii(2) = real(yii(2), fp)
		ys(2, 1) = ys(1, 1) + 0.5*dt * f1_f(yei(1), yei(2), dt * t, a) + 0.5*dt * f1_f(yeii(1), yeii(2), dt * t, a)
		ys(2, 2) = ys(1, 2) + 0.5*dt * f2_f(yei(1), yei(2), dt * t, a) + 0.5*dt * f2_f(yeii(1), yeii(2), dt * t, a)
		ys(1, 1) = ys(2, 1)
		ys(1, 2) = ys(2, 2)

	end do
	print *, "Average newton1 iterations: ", i_new_tot/n 
	write(2,*) ys(1, :)
	close(2)
	close(12)

end subroutine lobatto

subroutine rkf45(ti, tf, ys, dt, y0, a)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: ti, tf, y0(2), dt, a
	real(fp), intent(inout) :: ys(2, 2)
	real(fp) :: k0(2), k1(2), k2(2), k3(2), k4(2), k5(2), tmp(2)
	real(fp) :: rkf_a(6), rkf_b(6, 5), rkf_ch(6), rkf_ct(6)
	real(fp) :: h, h_new, t, eps, te
	logical :: last_step
	!character(2) :: x1 
	!character(15) :: fname
	!character(8) :: fmt 
	!integer :: sizef

	last_step = .false.
	t = ti
	h = dt 
	h_new = h  
	eps = epsilon(eps) * 2.0_fp
	ys(1, 1) = y0(1)
	ys(1, 2) = y0(2)

	rkf_a(1) = 0.0_fp
	rkf_a(2) = 2.0_fp/9.0_fp
	rkf_a(3) = 1.0_fp/3.0_fp
	rkf_a(4) = 3.0_fp/4.0_fp
	rkf_a(5) = 1.0_fp
	rkf_a(6) = 5.0_fp/6.0_fp

	rkf_b(2, 1) = 2.0_fp/9.0_fp
	rkf_b(3, 1) = 1.0_fp/12.0_fp
	rkf_b(3, 2) = 1.0_fp/4.0_fp
	rkf_b(4, 1) = 69.0_fp/128.0_fp
	rkf_b(4, 2) = -243.0_fp/128.0_fp
	rkf_b(4, 3) = 135.0_fp/64.0_fp
	rkf_b(5, 1) = -17.0_fp/12.0_fp
	rkf_b(5, 2) = 27.0_fp/4.0_fp
	rkf_b(5, 3) = -27.0_fp/5.0_fp
	rkf_b(5, 4) = 16.0_fp/15.0_fp
	rkf_b(6, 1) = 65.0_fp/432.0_fp
	rkf_b(6, 2) = -5.0_fp/16.0_fp
	rkf_b(6, 3) = 13.0_fp/16.0_fp
	rkf_b(6, 4) = 4.0_fp/27.0_fp
	rkf_b(6, 5) = 5.0_fp/144.0_fp

	rkf_ch(1) = 47.0_fp/450.0_fp
	rkf_ch(2) = 0.0_fp
	rkf_ch(3) = 12.0_fp/25.0_fp
	rkf_ch(4) = 32.0_fp/225.0_fp
	rkf_ch(5) = 1.0_fp/30.0_fp
	rkf_ch(6) = 6.0_fp/25.0_fp

	rkf_ct(1) = -1.0_fp/150.0_fp
	rkf_ct(2) = 0.0_fp
	rkf_ct(3) = 3.0_fp/100.0_fp
	rkf_ct(4) = -16.0_fp/75.0_fp
	rkf_ct(5) = -1.0_fp/20.0_fp
	rkf_ct(6) = 6.0_fp/25.0_fp

	!fmt = '(I2.2)'
	!sizef = sizeof(a)
	!write (x1, fmt) sizef
	!fname = 'ref_sol_'//trim(x1)//'.txt'

	!open(unit=2, file=fname, ACTION="write", STATUS="replace")
	open(unit=2, file='ref_sol.txt', ACTION="write", STATUS="replace")
	open(unit=12, file='ref_sol_t.txt', ACTION="write", STATUS="replace")
	!write(12,*) t
	!write(2,*) ys(1, :)
	do while (t <= tf)

		tmp(1) = ys(1, 1)
		tmp(2) = ys(1, 2)
		k0(1) = h * f1_f(tmp(1), tmp(2), t + rkf_a(1) * h, a)
		k0(2) = h * f2_f(tmp(1), tmp(2), t + rkf_a(1) * h, a)

		tmp(1) = ys(1, 1) + rkf_b(2, 1) * k0(1)
		tmp(2) = ys(1, 2) + rkf_b(2, 1) * k0(2)
		k1(1) = h * f1_f(tmp(1), tmp(2), t + rkf_a(2) * h, a)
		k1(2) = h * f2_f(tmp(1), tmp(2), t + rkf_a(2) * h, a)

		tmp(1) = ys(1, 1) + rkf_b(3, 1) * k0(1) + rkf_b(3, 2) * k1(1)
		tmp(2) = ys(1, 2) + rkf_b(3, 1) * k0(2) + rkf_b(3, 2) * k1(2)
		k2(1) = h * f1_f(tmp(1), tmp(2), t + rkf_a(3) * h, a)
		k2(2) = h * f2_f(tmp(1), tmp(2), t + rkf_a(3) * h, a)

		tmp(1) = ys(1, 1) + rkf_b(4, 1) * k0(1) + rkf_b(4, 2) * k1(1)&
			+ rkf_b(4, 3) * k2(1)
		tmp(2) = ys(1, 2) + rkf_b(4, 1) * k0(2) + rkf_b(4, 2) * k1(2)&
			+ rkf_b(4, 3) * k2(2)
		k3(1) = h * f1_f(tmp(1), tmp(2), t + rkf_a(3) * h, a)
		k3(2) = h * f2_f(tmp(1), tmp(2), t + rkf_a(3) * h, a)

		tmp(1) = ys(1, 1) + rkf_b(5, 1) * k0(1) + rkf_b(5, 2) * k1(1)&
			+ rkf_b(5, 3) * k2(1) + rkf_b(5, 4) * k3(1)
		tmp(2) = ys(1, 2) + rkf_b(5, 1) * k0(2) + rkf_b(5, 2) * k1(2)&
			+ rkf_b(5, 3) * k2(2) + rkf_b(5, 4) * k3(2)
		k4(1) = h * f1_f(tmp(1), tmp(2), t + rkf_a(3) * h, a)
		k4(2) = h * f2_f(tmp(1), tmp(2), t + rkf_a(3) * h, a)

		tmp(1) = ys(1, 1) + rkf_b(6, 1) * k0(1) + rkf_b(6, 2) * k1(1)&
			+ rkf_b(6, 3) * k2(1) + rkf_b(6, 4) * k3(1) + rkf_b(6, 5) * k4(1)
		tmp(2) = ys(1, 2) + rkf_b(6, 1) * k0(2) + rkf_b(6, 2) * k1(2)&
			+ rkf_b(6, 3) * k2(2) + rkf_b(6, 4) * k3(2) + rkf_b(6, 5) * k4(2)
		k5(1) = h * f1_f(tmp(1), tmp(2), t + rkf_a(3) * h, a)
		k5(2) = h * f2_f(tmp(1), tmp(2), t + rkf_a(3) * h, a)

		ys(2, 1) = ys(1, 1) + rkf_ch(1) * k0(1) + rkf_ch(2) * k1(1)&
			+ rkf_ch(3) * k2(1) + rkf_ch(4) * k3(1) + rkf_ch(5) * k4(1)&
			+ rkf_ch(6) * k5(1)
		ys(2, 2) = ys(1, 2) + rkf_ch(1) * k0(2) + rkf_ch(2) * k1(2)&
			+ rkf_ch(3) * k2(2) + rkf_ch(4) * k3(2) + rkf_ch(5) * k4(2)&
			+ rkf_ch(6) * k5(2)

		tmp(1) = rkf_ct(1) * k0(1) + rkf_ct(2) * k1(1)&
			+ rkf_ct(3) * k2(1) + rkf_ct(4) * k3(1) + rkf_ct(5) * k4(1)&
			+ rkf_ct(6) * k5(1)
		tmp(2) = rkf_ct(1) * k0(2) + rkf_ct(2) * k1(2)&
			+ rkf_ct(3) * k2(2) + rkf_ct(4) * k3(2) + rkf_ct(5) * k4(2)&
			+ rkf_ct(6) * k5(2)

		te = norm2(tmp)
		h_new = 0.9_fp * h * (eps / te) ** (1.0_fp/5.0_fp)
		!if (h_new > dt) then
		!	h_new = dt 
		!end if 

		if (last_step) then
			!last_step set to make sure we don't go passed tf
			!only write last step for ease of processing
			t = t + h 
			write(12,*) t
			ys(1, 1) = ys(2, 1)
			ys(1, 2) = ys(2, 2)
			write(2,*) ys(1, :)
			exit
		end if 
		!if it's not the last step continue on with rkf stuff
		if (te > eps) then
			! if te > eps repeat step at new h
			h = h_new
		else
			! else increment and start next step with new h 
			t = t + h 
			!write(12,*) t
			if (t + h_new > tf) then 
				!make sure we do not go passed tf.
				h = tf - t 
				last_step = .true.
			else
				h = h_new 
			end if
			ys(1, 1) = ys(2, 1)
			ys(1, 2) = ys(2, 2)
			!write(2,*) ys(1, :)
		end if 
	end do 
	close(2)
	close(12)

end subroutine rkf45

subroutine rk4(ti, tf, ys, dt, y0, a)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: ti, tf, y0(2), dt, a
	real(fp), intent(inout) :: ys(2, 2)
	real(fp) :: k1(2), k2(2), k3(2), k4(2), tmp(2)
	real(fp) :: h, t, te
	logical :: last_step
	!character(2) :: x1 
	!character(15) :: fname
	!character(8) :: fmt 
	!integer :: sizef

	last_step = .false.
	t = ti
	h = dt 
	ys(1, 1) = y0(1)
	ys(1, 2) = y0(2)

	!fmt = '(I2.2)'
	!sizef = sizeof(a)
	!write (x1, fmt) sizef
	!fname = 'ref_sol_'//trim(x1)//'.txt'

	!open(unit=2, file=fname, ACTION="write", STATUS="replace")
	open(unit=2, file='ref_sol.txt', ACTION="write", STATUS="replace")
	open(unit=12, file='ref_sol_t.txt', ACTION="write", STATUS="replace")
	!write(12,*) t
	!write(2,*) ys(1, :)
	do while (t <= tf)
		if (t + h > tf) then 
			!make sure we do not go passed tf.
			h = tf - t 
			last_step = .true. 
		end if

		tmp(1) = ys(1, 1)
		tmp(2) = ys(1, 2)
		k1(1) = f1_f(tmp(1), tmp(2), t, a)
		k1(2) = f2_f(tmp(1), tmp(2), t, a)

		tmp(1) = ys(1, 1) + h * 0.5_fp * k1(1)
		tmp(2) = ys(1, 2) + h * 0.5_fp * k1(2)
		k2(1) = f1_f(tmp(1), tmp(2), t + 0.5_fp * h, a)
		k2(2) = f2_f(tmp(1), tmp(2), t + 0.5_fp * h, a)

		tmp(1) = ys(1, 1) + h * 0.5_fp * k2(1)
		tmp(2) = ys(1, 2) + h * 0.5_fp * k2(2)
		k3(1) = f1_f(tmp(1), tmp(2), t + 0.5_fp * h, a)
		k3(2) = f2_f(tmp(1), tmp(2), t + 0.5_fp * h, a)

		tmp(1) = ys(1, 1) + h * k3(1)
		tmp(2) = ys(1, 2) + h * k3(2)
		k4(1) = f1_f(tmp(1), tmp(2), t + h, a)
		k4(2) = f2_f(tmp(1), tmp(2), t + h, a)

		ys(2, 1) = ys(1, 1) + 1.0_fp/6.0_fp * h&
			* (k1(1) + 2.0_fp * k2(1) + 2.0_fp * k3(1) + k4(1)) 
		ys(2, 2) = ys(1, 2) + 1.0_fp/6.0_fp * h&
			* (k1(2) + 2.0_fp * k2(2) + 2.0_fp * k3(2) + k4(2))
 
		ys(1, 1) = ys(2, 1)
		ys(1, 2) = ys(2, 2)
		!write(2,*) ys(1, :)
		t = t + h
		!write(12,*) t

		if (last_step) then 
			exit 
		end if 
 
	end do 
	write(2,*) ys(1, :)
	write(12,*) t
	close(2)
	close(12)

end subroutine rk4

end program main