import subprocess as sp 
import os
import numpy as np 
#import matplotlib.pyplot as plt 
import json
import itertools
import sys
import time

"""
Compile using included makefile
d_redu - The reduced precision data type {real32, real64, real128}
d_full - The full precision data type {same ^}

no return

Note: I did try using subprocess but make wouldn't use the variables.
so I just used os.system
"""
def compile(d_full, d_redu):
	cmd = f"make fp={d_full} rp={d_redu}"
	#print(cmd)
	#print(cmd.split())
	#sp.run(['make', f"full_prec='{d_full}'", f"redu_prec='{d_redu}'"], shell=True, check=True)
	#sp.run(cmd.split(), shell=True, check=True)
	stat = os.system(cmd)
	if stat != 0:
		print(f"ERROR: exit status {stat}")

"""
Run the program using perf
dt - Program argument for dt

program outputs the time to run the method, so capture it and return.
perf stores data in a csv file. There is a convenient python wrapper
script to parse it provided by arm. So run that, and collect the data
to return as well.
"""
def run_prog(dt, test_name, n_runs=5):
	t = 0
	e = 0
	core_pow = 0
	core_en = 0
	l2_pow = 0
	mem_pow = 0
	for i in range(n_runs):
		#run perf and collect method output
		perf_file = 'data.perf'
		cmd = f'perf stat -x, -o {perf_file} -e duration_time,r11,r1e0,r3e0,r3e8 ./{test_name}.x {dt}'
		out = sp.run(cmd.split(), capture_output=True)
		#print(out.stdout.split())
		lines = out.stdout.split()
		t = t + float(lines[2])
		e = e + float(lines[5])
		#run the post processing script to collect energy data
		cmd = f'../postproc_perf_energy.py ./{perf_file}'
		out = sp.run(cmd.split(), capture_output=True)
		lines = out.stdout.split()
		core_pow = core_pow + float(lines[10])
		core_en = core_en + float(lines[14])
		l2_pow = l2_pow + float(lines[19])
		mem_pow = mem_pow + float(lines[24])
		#sleep a little for cleaner data
		time.sleep(5)

	return t/n_runs, e/n_runs, core_pow/n_runs, core_en/n_runs, l2_pow/n_runs, mem_pow/n_runs

"""
Run the full experiment for one of the tests
test_name - the name of the test to run. Name must match the directory 
and teh name of the test.

Writes the results of the experiment inside the test directory to a 
json file
"""
def experiment(test_name):
	workdir = os.path.join('./', test_name)
	if not os.path.exists(workdir):
		print(f"experiment directory {test_name} doesn't exist. Returning...")
	os.chdir(workdir)

	dt_exponent_min = 1
	dt_exponent_max = 7
	d_types = ['real128', 'real64', 'real32', 'real16']
	combos = itertools.combinations_with_replacement(d_types, 2)
	dts = [1*10**(-i) for i in range(dt_exponent_min, dt_exponent_max+1)]
	
	runs = []
	for d_full, d_redu in combos:
		compile(d_full, d_redu)
		time.sleep(3) #sleep a little so compile doesnt interfere with data collection
		run = {'redu': d_redu, 'full': d_full, 'errs': [], 'ts': [], 'dt': [], 'core_pow': [], 'core_en': [], 'l2_pow': [], 'mem_pow': []}
		for dt in dts:
			print(f"Running {test_name} for dt={dt}")
			t, e, cp, ce, lp, mp = run_prog(dt, test_name)
			run['dt'].append(dt)
			run['errs'].append(e)
			run['ts'].append(t)
			run['core_pow'].append(cp)
			run['core_en'].append(ce)
			run['l2_pow'].append(lp)
			run['mem_pow'].append(mp)
		runs.append(run)

	with open(f'energy_aarch64_{test_name}.json', 'w') as f:
		json.dump(runs, f)
	os.chdir('..')

def main():
	tests = sys.argv[1:]
	#tests = ['impmid', 'impmid-1p', 'impmid-2p', 'sdirk', 'sdirk-1p', 'sdirk-2p', 'sdirk-3p', 'novel-4s3pA', 'novel-4s3pB', 'novel-4s3pC']
	for test in tests:
		experiment(test)

if __name__ == '__main__':
	main()