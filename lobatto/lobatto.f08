!gfortran lobatto.f08 -o lobatto -cpp -DFULL_TYPE=real128 -DREDU_TYPE=real32
! Full and reduced precision type defaults
#ifndef FULL_TYPE
#define FULL_TYPE real64
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real32
#endif

program main
	!Declare the full and reduced precision type names.
	!remember for literal constants to put 1.0_fp to cast correctly
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE, qp=>real128
	implicit none
	character(len=32) :: arg
	!dt, initial time, final time, a
	real(fp) :: dt, ti=0.0, tf=1.0, a=1.0
	real :: start, finish !start/finish times for method
	real(fp) :: y0(2) !initial condition
	!ys method (yn and ynp1 only), ys analytical (yf only)
	real(fp) :: ys(2, 2)
	real(qp) :: ye(2)
	integer(kind=16) :: n

	!get command line args and then convert
	!https://gcc.gnu.org/onlinedocs/gfortran/GETARG.html
	!https://riptutorial.com/fortran/example/26615/passing-command-line-arguments
	call getarg(1, arg)
	read(arg, *)dt 
	!print *, dt

	!initial condition 
	y0(1) = 2.0_fp
	y0(2) = 0.0_fp

	if (sizeof(tf) == 16) then
		n = (tf - ti) / dt 
	else
		n = ceiling((tf - ti) / dt)
	end if
	
	call cpu_time(start)
	call lobatto(ti, ys, n, dt, y0, a)
	call cpu_time(finish)
	print *, "method duration: ", finish-start
	
	!print *, "Solution final point: ", ys(1, 1), ys(1, 2)
	open(unit=2, file="../ref_solution/ref_sol.txt")
	read (2,*) ye(1), ye(2)
	print *, "method error: ", abs(ye(1) - real(ys(1, 1), qp))


!Why this contains statement? see last answer and comments of
!https://stackoverflow.com/questions/16741252/function-return-type-mismatch
!links to useful
!http://www.pcc.qub.ac.uk/tec/courses/f77tof90/stu-notes/f90studentMIF_4.html
contains

!Problem Functions
!y1 - variable 1
!y2 - variable 2
!t - current time
!a - problem constant
!f - return value
function f1_f(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: y1, y2, t, a 
	real(fp) :: val 
	val = y2 
end function f1_f

function f2_f(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: y1, y2, t, a 
	real(fp) :: val
	val = a * (1.0_fp - y1 * y1) * y2 - y1 
end function f2_f

function f1_r(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: y1, y2, t, a 
	real(rp) :: val 
	val = y2 
end function f1_r

function f2_r(y1, y2, t, a) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: y1, y2, t, a 
	real(rp) :: val
	val = a * (1.0_rp - y1 * y1) * y2 - y1 
end function f2_r

subroutine inverse(a,c,n)
!============================================================
! Inverse matrix
! Method: Based on Doolittle LU factorization for Ax=b
! Alex G. December 2009
! shamelessly borrowed from https://ww2.odu.edu/~agodunov/computing/programs/book2/Ch06/Inverse.f90
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! n      - dimension
! output ...
! c(n,n) - inverse matrix of A
! comments ...
! the original matrix a(n,n) will be destroyed 
! during the calculation
!===========================================================
use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
implicit none 
integer n
real(rp) :: a(n,n), c(n,n)
real(rp) :: L(n,n), U(n,n), b(n), d(n), x(n)
real(rp) :: coeff
integer i, j, k

! step 0: initialization for matrices L and U and b
! Fortran 90/95 aloows such operations on matrices
L=0.0
U=0.0
b=0.0

! step 1: forward elimination
do k=1, n-1
   do i=k+1,n
      coeff=a(i,k)/a(k,k)
      L(i,k) = coeff
      do j=k+1,n
         a(i,j) = a(i,j)-coeff*a(k,j)
      end do
   end do
end do

! Step 2: prepare L and U matrices 
! L matrix is a matrix of the elimination coefficient
! + the diagonal elements are 1.0
do i=1,n
  L(i,i) = 1.0
end do
! U matrix is the upper triangular part of A
do j=1,n
  do i=1,j
    U(i,j) = a(i,j)
  end do
end do

! Step 3: compute columns of the inverse matrix C
do k=1,n
  b(k)=1.0
  d(1) = b(1)
! Step 3a: Solve Ld=b using the forward substitution
  do i=2,n
    d(i)=b(i)
    do j=1,i-1
      d(i) = d(i) - L(i,j)*d(j)
    end do
  end do
! Step 3b: Solve Ux=d using the back substitution
  x(n)=d(n)/U(n,n)
  do i = n-1,1,-1
    x(i) = d(i)
    do j=n,i+1,-1
      x(i)=x(i)-U(i,j)*x(j)
    end do
    x(i) = x(i)/u(i,i)
  end do
! Step 3c: fill the solutions x(n) into column k of C
  do i=1,n
    c(i,k) = x(i)
  end do
  b(k)=0.0
end do
end subroutine inverse

subroutine jacobian(x1, x2, y1, y2, a, dt, J)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(rp), intent(in) :: x1, x2, y1, y2, a, dt 
	real(rp), intent(inout) :: J(8, 4)

	J(1, 1) = -1.0_rp 
	J(1, 2) = 0.5_rp * dt 
	J(1, 3) = 0.0_rp 
	J(1, 4) = -0.5_rp * dt 
	J(2, 1) = 0.0_rp 
	J(2, 2) = 0.5_rp * dt - 1.0_rp 
	J(2, 3) = 0.0_rp 
	J(2, 4) = -0.5_rp * dt 
	J(3, 1) = 0.0_rp
	J(3, 2) = 0.5_rp * dt 
	J(3, 3) = -1.0_rp 
	J(3, 4) = 0.5_rp * dt 
	J(4, 1) = 0.0_rp 
	J(4, 2) = 0.5_rp * dt 
	J(4, 3) = 0.0_rp 
	J(4, 4) = 0.5_rp * dt - 1

	J(5, 1) = a * dt * x1 * x2 - 2.0_rp 
	J(5, 2) = 0.5_rp * a * dt * (1.0_rp - x1 * x1)
	J(5, 3) = -a * dt * y1 * y2 - 1.0_rp 
	J(5, 4) = -0.5_rp * a * dt * (1.0_rp - y1 * y1)
	J(6, 1) = a * dt * x1 * x2 - 1.0_rp 
	J(6, 2) = 0.5_rp * a * dt * (1.0_rp - x1 * x1) - 1.0_rp
	J(6, 3) = -a * dt * y1 * y2 - 1.0_rp 
	J(6, 4) = -0.5_rp * a * dt * (1.0_rp - y1 * y1)
	J(7, 1) = a * dt * x1 * x2 - 1.0_rp 
	J(7, 2) = 0.5_rp * a * dt * (1.0_rp - x1 * x1)
	J(7, 3) = a * dt * y1 * y2 - 2.0_rp 
	J(7, 4) = 0.5_rp * a * dt * (1.0_rp - y1 * y1)
	J(8, 1) = a * dt * x1 * x2 - 1.0_rp 
	J(8, 2) = 0.5_rp * a * dt * (1.0_rp - x1 * x1)
	J(8, 3) = a * dt * y1 * y2 - 1.0_rp 
	J(8, 4) = 0.5_rp * a * dt * (1.0_rp - y1 * y1) - 1.0_rp
end subroutine jacobian

subroutine newtons(t, h, y, x0, y0, a)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t, h, a 
	real(rp), intent(in) :: y(2)
	real(rp), intent(inout) :: x0(2), y0(2)
	real(rp) :: tol, norm
	real(rp) :: Fn(8), Fni(4), Jn(8, 4), Jn_(4, 8), Jni(4, 4)
	integer :: i, max_iter=5000

	tol = epsilon(tol) * 1.001_rp

	do i=0, max_iter-1
		Fn(1) = y(1) + h/2.0_rp * f1_r(x0(1), x0(2), t, a) - h/2.0_rp * f1_r(y0(1), y0(2), t, a) - x0(1)
		Fn(2) = y(2) + h/2.0_rp * f1_r(x0(1), x0(2), t, a) - h/2.0_rp * f1_r(y0(1), y0(2), t, a) - x0(2)
		Fn(3) = y(1) + h/2.0_rp * f1_r(x0(1), x0(2), t, a) + h/2.0_rp * f1_r(y0(1), y0(2), t, a) - y0(1)
		Fn(4) = y(2) + h/2.0_rp * f1_r(x0(1), x0(2), t, a) + h/2.0_rp * f1_r(y0(1), y0(2), t, a) - y0(2)
		Fn(5) = y(1) + h/2.0_rp * f2_r(x0(1), x0(2), t, a) - h/2.0_rp * f2_r(y0(1), y0(2), t, a) - x0(1)
		Fn(6) = y(2) + h/2.0_rp * f2_r(x0(1), x0(2), t, a) - h/2.0_rp * f2_r(y0(1), y0(2), t, a) - x0(2)
		Fn(7) = y(1) + h/2.0_rp * f2_r(x0(1), x0(2), t, a) + h/2.0_rp * f2_r(y0(1), y0(2), t, a) - y0(1)
		Fn(8) = y(2) + h/2.0_rp * f2_r(x0(1), x0(2), t, a) + h/2.0_rp * f2_r(y0(1), y0(2), t, a) - y0(2)
		norm = norm2(Fn) !norm2 is an intrinsic function
		if (norm < tol) then
			return !converged so can leave
		end if 

		call jacobian(x0(1), x0(2), y0(1), y0(2), a, h, Jn)
		call inverse(matmul(transpose(Jn), Jn), Jni, 4) !transpose is an intrinsic function
		Jn_ = matmul(Jni, transpose(Jn))
		Fni = matmul(Jn_, Fn)
		x0(1) = x0(1) - Fni(1)
		x0(2) = x0(2) - Fni(2)
		y0(1) = y0(1) - Fni(3)
		y0(2) = y0(2) - Fni(4)
	end do

end subroutine newtons

subroutine lobatto(ti, ys, n, dt, y0, a)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=16), intent(in) :: n
	real(fp), intent(in) :: ti, y0(2)
	real(fp), intent(in) :: dt, a
	real(fp), intent(inout) :: ys(2, 2)
	integer(kind=16) :: t 
	real(rp) :: t_cur
	real(rp) :: yi(2), yii(2), y(2)
	real(fp) :: yei(2), yeii(2)

	ys(1, 1) = y0(1)
	ys(1, 2) = y0(2)
	t_cur = real(ti, rp)

	do t=1,n
		yi(1) = real(ys(1, 1), rp)
		yi(2) = real(ys(1, 2), rp)
		yii(1) = real(ys(1, 1), rp)
		yii(2) = real(ys(1, 2), rp)
		y(1) = real(ys(1, 1), rp)
		y(2) = real(ys(1, 2), rp)

		t_cur = dt * t
		call newtons(t_cur, real(dt, rp), y, yi, yii, real(a, rp))
		yei(1) = real(yi(1), fp) !up cast
		yei(2) = real(yi(2), fp)
		yeii(1) = real(yii(1), fp)
		yeii(2) = real(yii(2), fp)
		ys(2, 1) = ys(1, 1) + 0.5*dt * f1_f(yei(1), yei(2), dt * t, a) + 0.5*dt * f1_f(yeii(1), yeii(2), dt * t, a)
		ys(2, 2) = ys(1, 2) + 0.5*dt * f2_f(yei(1), yei(2), dt * t, a) + 0.5*dt * f2_f(yeii(1), yeii(2), dt * t, a)
		ys(1, 1) = ys(2, 1)
		ys(1, 2) = ys(2, 2)

	end do
	!print *, t_cur

end subroutine lobatto

end program main