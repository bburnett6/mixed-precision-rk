import subprocess as sp 
import os
import numpy as np 
#import matplotlib.pyplot as plt 
import json
import itertools
import multiprocessing as mp 

"""
Compile using included makefile
d_redu - The reduced precision data type {real32, real64, real128}
d_full - The full precision data type {same ^}

no return

Note: I did try using subprocess but make wouldn't use the variables.
so I just used os.system
"""
def compile(d_full, d_redu):
	cmd = f"make fp='{d_full}' rp='{d_redu}'"
	#print(cmd)
	#print(cmd.split())
	#sp.run(['make', f"full_prec='{d_full}'", f"redu_prec='{d_redu}'"], shell=True, check=True)
	#sp.run(cmd.split(), shell=True, check=True)
	stat = os.system(cmd)
	if stat != 0:
		print(f"ERROR: exit status {stat}")

"""
Run the program
dt - Program argument for dt

program outputs the time to run the method, so capture it and return
"""
def run_prog(dt, test_name, n_runs=5):
	t = 0
	e = 0
	for i in range(n_runs):
		out = sp.run([f'./{test_name}.x', f'{dt}'], capture_output=True)
		#print(out.stdout.split())
		lines = out.stdout.split()
		t = t + float(lines[2])
		e = e + float(lines[5])
	return t/n_runs, e/n_runs

"""
Run the full experiment for one of the tests
test_name - the name of the test to run. Name must match the directory 
and teh name of the test.

Writes the results of the experiment inside the test directory to a 
json file
"""
def experiment(test_name):
	workdir = os.path.join('./', test_name)
	if not os.path.exists(workdir):
		print(f"experiment directory {test_name} doesn't exist. Returning...")
	os.chdir(workdir)

	dt_exponent_min = 1
	dt_exponent_max = 7
	d_types = ['real128', 'real64', 'real32', 'real16']
	combos = itertools.combinations_with_replacement(d_types, 2)
	dts = [1*10**(-i) for i in range(dt_exponent_min, dt_exponent_max+1)]
	
	runs = []
	for d_full, d_redu in combos:
		compile(d_full, d_redu)
		r = {'redu': d_redu, 'full': d_full, 'errs': [], 'ts': []}
		for dt in dts:
			print(f"Running {test_name} for dt={dt}")
			t, e = run_prog(dt, test_name)
			r['errs'].append(e)
			r['ts'].append(t)
		runs.append(r)

	with open(f'runs_aarch64_{test_name}.json', 'w') as f:
		json.dump(runs, f)

def main():
	#Use multiprocess to run all tests at once. 
	#Theoretically this shouldn't impact timing results because the 
	#tests are all serial.
	tests = ['impmid', 'impmid-1p', 'impmid-2p', 'sdirk', 'sdirk-1p', 'sdirk-2p', 'sdirk-3p', 'novel-4s3pA', 'novel-4s3pB', 'novel-4s3pC']
	with mp.Pool(processes=len(tests)) as pool:
		pool.map(experiment, tests)

if __name__ == '__main__':
	main()